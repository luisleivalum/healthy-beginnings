<?php
//funciones para mysql

/**
* Abre la conexion con mysql
*/
function _conectDB(){

    require("conf/config.php");
    $strHost = (isset($dbConfig["host"]))?$dbConfig["host"]:"";
    $strUser = (isset($dbConfig["user"]))?$dbConfig["user"]:"";
    $strPass = (isset($dbConfig["password"]))?$dbConfig["password"]:"";
    $strDbName = (isset($dbConfig["db_name"]))?$dbConfig["db_name"]:"";
    
    if($strHost != "" && $strUser != "" && $strDbName != ""){
    
        @$link = mysqli_connect($strHost, $strUser, $strPass, $strDbName);
        if (!$link) {
            if($fh = fopen('logmysql.txt','w')){
                $stringData = "MySQL Error";
                fwrite($fh, $stringData,1024);
                fclose($fh);
            }
        }
        else{
            return $link;
        }  
    }
}

/**
* Cierra la conexion a la base de datos
*/
function _closeConectDB(){
    global $dbLink;
    mysqli_close($dbLink);
}

/**
* Realiza una consulta a la base de datos
*/
function _do_query($strQuery = ""){
    global $dbLink;
    
    if($strQuery != ""){
        
        @$src = mysqli_query($dbLink, $strQuery);
        if($src){
            return $src;
        }
        else{
            @print  mysql_error($dbLink);
            return false;
        }
    }
}

/**
* Retorna el recurso de mysqli_fetch_assoc
*/
function _assoc($qTMP = ""){
    if($qTMP){
        return mysqli_fetch_assoc($qTMP);
    }
    else{
        return false;
    }
}

/**
* Retorna el numero de filas devuelto por un query
*/
function _number_rows($qTMP = ""){
    if($qTMP){
        return mysqli_num_rows($qTMP);
    }
    else{
        return false;
    }
}

function _lastID(){
    global $dbLink;
    
    return mysqli_insert_id($dbLink);
}

/**
* Retorna el array devuelto por un query, esto con fines de DEBUG
*/
function _debug($strQuery = ""){
    $qTMP = _do_query($strQuery);
    if($qTMP){
        if(_number_rows($qTMP) > 0){
            while($arrTMP = _assoc($qTMP)){
                bin_debug($arrTMP);   
            }
        }
        else{
            bin_debug("0 filas");
        }
    }
}

/**
* Escapea variables para enviarlas en un query
*/
function _escape($var = false){
    global $dbLink;
    
    if($var){
        return mysqli_escape_string($dbLink, $var);   
    }
    else{
        return false;
    }
}

function _response($strQuery = "", $boolPrintResponse = false){
    
    if($strQuery != ""){
        
        if($boolPrintResponse){
            if(_do_query($strQuery)){
                print "ok";
            }   
            else{
                print "fail";
            }
        }
        else{
            if(_do_query($strQuery)){
                return "ok";
            }   
            else{
                return "fail";
            }
        }   
    }
}

function _freeResult($qTMP = false){
    global $dbLink;
    
    if($qTMP){
        return mysqli_free_result($qTMP);   
    }
    else{
        return false;
    }
}